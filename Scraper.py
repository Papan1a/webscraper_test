#coding=cp1251
__author__ = 'Provalov P.V.'

# �����������
from lxml.html import fromstring
from lxml import etree
import json
import requests
import re

URL = 'http://www.flyniki.com/en-RU/start.php'
DATEOVERVIEW = False
RESPONSE_ERROR = False

class session:
    URL = 'http://www.flyniki.com/en-RU/start.php'
    def __init__(self):
        self.departure_str = ''
        self.distanation_str = ''
        self.outbound_date_str = ''
        self.return_date_str = ''
        self.one_way_str = ''

def get_session(url, payloads):
    '''
        ������� ������ ������, �������� ����������� ������� �� URL,
        ������������ responses,
        returns
    '''
    global DATEOVERVIEW
    global RESPONSE_ERROR
    try:
        # 1) �������� ������, � �������� ��������� ����������
        with requests.Session() as c:
            # 1 request: ���� POST, �� ���� �������� ��� payload
            r1 = c.request('POST', url, data= payloads[0], allow_redirects= False)
            # �� headers �������� ��������
            location= r1.headers.get('location')
            # 2 request: ���� GET, �.�. �� ���� ��������� �� ���������, �� response �������� SID
            r2 = c.request('GET',location,allow_redirects= False)
            # 3 request: ����� ��� ����� ��� �� ������, ��� ��������� ������������ � response - ���� ��
            # �������, ��� ������ ��� ����������� ���� ���������� � ��� ���-�� ����������
            r3 = c.get('http://www.flyniki.com'+r2.headers.get('location'))
            # 4 request: ��������� POST, � ������� �� �������� ajax_payload
            # �� ���-��� ��� ���� ����� ��� ����������� �� r3 � payload
            # ���������� 4 response � ����� ���, ��� ��� ���� ����� �������� �� �������
            r4 = c.request('POST'
                           ,'http://www.flyniki.com'+r2.headers.get('location')
                           ,data= payloads[1]
                           )
            try:
                infos = json.loads(r4.text)[u'templates'][u'infos']
                priceoverview = json.loads((r4.text))[u'templates'][u'priceoverview']
                if len(infos) <= 1 and len(priceoverview) <= 1:
                    r5 = c.request('POST'
                                   ,'http://www.flyniki.com'+r2.headers.get('location')
                                   ,data= payloads[2])
                    out = json.loads(r5.text)[u'templates'][u'dateoverview']
                    DATEOVERVIEW = True
                else:
                    out = json.loads(r4.text)[u'templates'][u'main']
                return out
            except (ValueError, KeyError, TypeError):
                try:
                    error = json.loads(r4.text)[u'error']
                    RESPONSE_ERROR = True
                    return error
                except (ValueError, KeyError, TypeError):
                    print "JSON format error"
    except requests.ConnectionError:
        print('Error http connection to http://www.flyniki.com/')

def parser(str):
    '''
        ������� �������� ������, ������ �,
        ������� ������ document_fromstring �� �������� ������ ���������
        ������ ������� ������ ��� �������� � ����� �� ��������
    '''
    global DATEOVERVIEW
    global RESPONSE_ERROR
    html_page = fromstring(str)
    out = []
    if DATEOVERVIEW == True:
        dateoverview_table_thead = html_page.xpath('.//div[@class="wrapper"]//thead//th/text()')
        dateoverview_table = html_page.xpath('.//div[@class="wrapper"]//tbody/tr')
        row_list = [dateoverview_table[i].xpath('./child::td[@class="nooffer" or @class="offer" or @class="offer cheapest"]/descendant::text()') for i in xrange(len(dateoverview_table))]
#                                                      or @class="offer cheapest \
 #                                                     or @class="offer")]/descendant-or-self::text()')

        print row_list
    elif DATEOVERVIEW == True:
        pass
    else:
        outbound_table = html_page.xpath('.//div[@class="outbound block"]//table[@class="flighttable"]/tbody\
                                        /tr[(@class="flightrow")or(@class="flightrow selected")]')
        time_list = [outbound_table[i].xpath('.//td[@class = "table-text-left"]/span/descendant::text()')
                     for i in xrange(len(outbound_table))]
        price_list = [outbound_table[i].xpath('.//div[@class = "current"]/span/text()')
                      for i in xrange(len(outbound_table))]
        outbound_out = [time_list[i] + price_list[i] for i in xrange(len(price_list))]

        return_table = html_page.xpath('.//div[@class="return block"]//table[@class="flighttable"]/tbody\
                                        /tr[(@class="flightrow")or(@class="flightrow selected")]')
        time_list = [return_table[i].xpath('.//td[@class = "table-text-left"]/span/descendant::text()')
                     for i in xrange(len(return_table))]
        price_list = [return_table[i].xpath('.//div[@class = "current"]/span/text()')
                      for i in xrange(len(return_table))]
        return_out = [time_list[i] + price_list[i] for i in xrange(len(price_list))]
        out = [outbound_out,return_out]
    return out

def check_input(out_code, in_code, out_date, ret_date=None):
    '''
    ��������� �������� ��������� �� ������������
    :input:
        out_code - IATA ��� ��������� ������
        in_code - IATA ��� ��������� ��������
        out_date - ���� ������
        ret_date - ���� ��������, optional, ���� �� �������, �� � ���� �������
    :return:
        � ������ ������ ���������� ������ � �������
        � ��������� ������ ������ ������ 'OK'
    '''
    def check_date(date_string):
        '''
            ������� ��������� ������������ ����� ���� � ������� yyyy-mm-dd
           :returns:
           0 - ��� ��
           1 - ������ ������� yyyy-mm-dd
           2 - ������ �������� ���, �����, ����
        '''
        if not re.match('^[0-9]{4}-[0-9]{2}-[0-9]{2}$', date_string):
          #  print 'Date must be in \'yyyy-mm-dd\' format'
            return 1
            # �������� �� ������������ �������� ���, �����, ����
        date_list = date_string.split('-')
        err = False
        if int(date_list[0]) < 2015:
           # print 'Incorrect year: ', date_list[0]
            err = True
        elif int(date_list[1]) > 12:
           # print 'Incorrect month: ', date_list[1]
            err = True
        elif int(date_list[2]) > 31:
            err = True
           # print 'Incorrect number: ', date_list[2]
        if err:
            return 2
        return 0
    out = 'OK'
    if re.match('^[A-Z]{3}$',out_code) is None:
        out = 'Error outbound IATA code format'
    elif re.match('^[A-Z]{3}$',in_code) is None:
        out = 'Error inbound IATA code format'
    elif check_date(out_date) == 1:
        out = 'Error outbound_date format.'
    elif check_date(out_date) == 2:
        out = 'Error outbound_date data.'
    elif ret_date is None:
        pass
    elif check_date(ret_date) == 1:
        out = 'Error inbound_date format.'
    elif check_date(ret_date) == 2:
        out = 'Error inbound_date data.'
    return out

def create_dict(out_code, in_code, out_date, ret_date=None):
    '''
    ������� ������ �� ���� �������� c �������, ������� ����� ���������� �� ������
    :input:
        out_code - IATA ��� ��������� ������
        in_code - IATA ��� ��������� ��������
        out_date - ���� ������
        in_date - ���� ��������, optional, ���� �� �������, �� � ���� �������
    :return:
        ������ �� ���� ��������
    '''
    one_way = '0'
    if ret_date is None: one_way = '1'
    out = [
        {
        'market'             : 'RU'
        ,'language'          : 'ru'
        ,'bookingmask_widget_id':'bookingmask-widget-stageoffer'
        ,'bookingmask_widget_dateformat':'dd.mm.yy'
        ,'departure'         : out_code
        ,'destination'       : in_code
        ,'outbounDate'       : out_date
        ,'returnDate'        : ret_date
        ,'oneway'            : one_way
        ,'openDateOverview'  : '0'
        ,'adulCount'         : '1'
        ,'childCount'        : '0'
        ,'infantCount'       : '0'
        },
        {
        '_ajax[templates][]':['main','priceoverview','infos','flightinfo']
        ,'_ajax[requestParams][departure]':out_code
        ,'_ajax[requestParams][destination]':in_code
        ,'_ajax[requestParams][returnDeparture]':''
        ,'_ajax[requestParams][returnDestination]':''
        ,'_ajax[requestParams][outboundDate]':out_date
        ,'_ajax[requestParams][returnDate]':ret_date
        ,'_ajax[requestParams][adultCount]':'1'
        ,'_ajax[requestParams][childCount]':'0'
        ,'_ajax[requestParams][infantCount]':'0'
        ,'_ajax[requestParams][openDateOverview]':''
        ,'_ajax[requestParams][oneway]':one_way
        },
        {
        '_ajax[templates][]':'dateoverview'
        }

    ]
    return out

def out_print(list, out_code, in_code, out_date, ret_date=None):
    output = out_code + ' - ' + in_code + ' ' + out_date + ' Outbound:\n'
    for i in xrange(len(list[0])):
        for j in xrange(len(list[0][i])):
            if list[0][i][j]!='':
                output += list[0][i][j]+'   '
        output += '\n'
    if list[1]!=[]:
        output += '\n' + in_code + ' - ' + out_code + ' ' + ret_date + ' Return:\n'
        for i in xrange(len(list[1])):
            for j in xrange(len(list[1][i])):
                if list[1][i][j]!='':
                    output += list[1][i][j]+'   '
            output += '\n'
    print output

def main(outbound_IATA, inbound_IATA, outbound_date, return_date=None):
    '''
    :input:
        outbound_IATA - IATA ��� ��������� ������
        inbound_IATA - IATA ��� ��������� ��������
        outbound_date - ���� ������
        inbound_date - ���� ��������, optional, ���� �� �������, �� � ���� �������
    :return:
        � ����������� �����
        ������ ����� ������ � ��������
        ������������ �������
        �������� ���, � ��������� ������
    '''

    # 1) ��������� �������� ������
    outbound_IATA = outbound_IATA.upper()
    inbound_IATA = inbound_IATA.upper()

    out1 = check_input(outbound_IATA, inbound_IATA, outbound_date, return_date)
    if out1 != 'OK':
        print out1
        return 0

    # 2) ������������ payload �������
    payloads = create_dict(outbound_IATA, inbound_IATA, outbound_date, return_date)

    # 3) ��������� ������� �� ������, �������� ������
    response = get_session(URL, payloads)

    # 4) ������ �����
    out = parser(response)

    # 5) ������� ���������
   # out_print(out, outbound_IATA, inbound_IATA, outbound_date, return_date)

if __name__ == '__main__':
    main('DME', 'HKG', '2015-09-13', '2015-09-13')
#DME - ������
#HKG - �������
#DRS - �������
